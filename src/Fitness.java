/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ilhammaziz
 */
public class Fitness {
    static byte[] solution = new byte[10];

    public static void setSolution(byte[] newSolution) {
        solution = newSolution;
    }

    public static void setSolution(String newSolution) {
        solution = new byte[newSolution.length()];
        for (int i = 0; i < newSolution.length(); i++) {
            String chr = newSolution.substring(i, i + 1);
            if (chr.contains("0") || chr.contains("1")) {
                solution[i] = Byte.parseByte(chr);
            } else {
                solution[i] = 0;
            }
        }
    }

    static int getFitness(Individual individual) {
        int fitness = 0;
        for (int i = 0; i < individual.getSize() && i < solution.length; i++) {
            if (individual.getGene(i) == solution[i]) {
                fitness++;
            }
        }
        return fitness;
    }
}

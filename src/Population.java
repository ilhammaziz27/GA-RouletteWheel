/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ilhammaziz
 */
public final class Population {
    Individual[] indvs;
    
    public Population(int popSize,boolean initializing){
        indvs = new Individual[popSize];
        
        if(initializing){
            for(int i=0;i<indvs.length;i++){
                Individual newIndv=new Individual();
                newIndv.generateIndividual();
                appendIndividual(i, newIndv);
            }
        }
    }
    
    public void appendIndividual(int index,Individual indv){
        indvs[index]=indv;
    }
    
    public Individual getIndividu(int index){
        return indvs[index];
    }
    
    public int getSize(){
        return indvs.length;
    }
}

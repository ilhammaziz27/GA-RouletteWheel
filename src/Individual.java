/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ilhammaziz
 */
public class Individual {
    static int defGenLength = 10;
    private final byte[] genes = new byte[defGenLength];
    
    private int fitness = 0;
    
    public void generateIndividual(){
        for(int i=0;i<this.genes.length;i++){
            byte gene = (byte) Math.round(Math.random());
            genes[i]=gene;
        }
    }
    
    public static void setDefGenLength(int length){
        defGenLength=length;
    }
    
    public int getGene(int index){
        return this.genes[index];
    }
    
    public void setGene(int index, byte value){
        genes[index] = value;
        fitness = 0;
    }
    
    public int getSize(){
        return this.genes.length;
    }
    
    public int getFitness(){
        if(this.fitness == 0){
            fitness = Fitness.getFitness(this);
        }
        return this.fitness;
    }
    
    @Override
    public String toString() {
        String geneString = "";
        for (int i = 0; i < genes.length; i++) {
            geneString += getGene(i);
        }
        return geneString;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ilhammaziz
 */
public class Tools {
    public Individual RouletteWheel(Population pop){
        int totalSum=0;
        for(int i=0;i<pop.getSize();i++){
            totalSum += pop.getIndividu(i).getFitness();
        }
        
        int rand=(int) (Math.random() * totalSum);
        int partialSum=0;
        for(int i=0;i<pop.getSize();i++){
            partialSum += pop.getIndividu(i).getFitness();
            if(partialSum >= rand){
                System.out.println("Selected Indvidual : "+i);
                return pop.getIndividu(i);
            }
        }
        
        return null;
    }
}
